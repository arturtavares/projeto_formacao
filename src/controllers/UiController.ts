import DOM from "../views/DOM";

export const loadCreateContactPage = () => {
  DOM.pageSearch.hidden = true;
  DOM.pageCreate.hidden = false;
  DOM.email.value = DOM.searchEmail.value;
  DOM.mobile.value = DOM.searchMobile.value;
  DOM.btnSearch.disabled = false;
};
