import DOM from "../views/DOM";

declare const ZOHO: any;

// CODIGO COM THEN
// export const getInitialData = () => {
//     // Get Current User Info
//     ZOHO.CRM.CONFIG.getCurrentUser().then(function(data){
//         const user = data;
//         ZOHO.CRM.CONFIG.getOrgInfo().then(function(data){
//             const org = data;
//             return {user, org}
//         });
//     });
// }

export const getInitialData = async () => {
  // Get Current User Info
  const userInfo = await ZOHO.CRM.CONFIG.getCurrentUser();
  // Get Org Info
  const orgInfo = await ZOHO.CRM.CONFIG.getOrgInfo();

  return {
    userInfo: userInfo.users[0],
    orgInfo: orgInfo.org[0],
  };
};

const validateEmail = (email) => {
  if (!email) return null;
  const emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  const nonLatinCode = /[^\u0000-\u00ff]/;
  const onlyLetters = /^[A-Za-z]+$/;
  return emailRegex.test(email) &&
    !nonLatinCode.test(email) &&
    !email.endsWith(Number()) &&
    onlyLetters.test(email[0])
    ? email.toLowerCase()
    : null;
};

const validateMobile = (mobile) => {
  if (!mobile) return null;
  const mobileRegex = /^[0-9]{9}$/;
  return mobileRegex.test(mobile) ? mobile : null;
};

export const getAndValidateValues = () => {
  // get email and mobile values
  const emailValue = DOM.searchEmail.value;
  const mobileValue = DOM.searchMobile.value;
  // Validate Values
  const email = validateEmail(emailValue);
  const mobile = validateMobile(mobileValue);
  if (!email || !mobile) {
    return false;
  }
  return { email, mobile };
};

export const searchRecords = async (data) => {
  const request = {
    url:
      "https://www.zohoapis.com/crm/v2/functions/widget_searchrecords/actions/execute?auth_type=apikey&zapikey=1003.3a439ac339985bfa708746af03ed5a54.22a944717175328f47dbd7c98f929e14",
    params: {
      data: JSON.stringify(data),
      module: "Contacts",
    },
  };
  try {
    const resp = JSON.parse(await ZOHO.CRM.HTTP.post(request));
    const code = resp.code;
    if (!code || code !== "success") {
      return { status: "error", message: resp.message };
    }
    return { status: "success", data: JSON.parse(`[${resp.details.output}]`) };
  } catch (error) {
    return { status: "error", message: error };
  }
};
