import {
  getAndValidateValues,
  getInitialData,
  searchRecords,
} from "./controllers/DataController";
import state from "./controllers/StateController";
import { loadCreateContactPage } from "./controllers/UiController";
import DOM from "./views/DOM";

declare const ZOHO: any;

const eventListeners = () => {
  DOM.formSearch.addEventListener("submit", async (e) => {
    e.preventDefault();
    DOM.btnSearch.disabled = true;
    // Get and Validate Records
    const data = getAndValidateValues();
    if (!data) {
      alert("Por favor insira um email e telemóvel válidos");
      DOM.btnSearch.disabled = false;
      return;
    }
    // Search Records
    const searchResp = await searchRecords(data);
    if (searchResp.status === "error") {
      alert("Ocorreu um erro, tente novamente mais tarde");
      DOM.btnSearch.disabled = false;
      return;
    }
    // Se os registos existem, nao deixamos criar
    if (searchResp.data.length > 0) {
      alert("Já existem registos");
      DOM.btnSearch.disabled = false;
      return;
    }
    // Se nao existem, deixamos criar
    return loadCreateContactPage();
  });
};

// Todos os dados necessários para iniciar o widget, vão estar aqui no init
const init = async () => {
  // Get Initial Data
  const data = await getInitialData();
  // Store Initial Data On State
  state.userInfo = data.userInfo;
  state.orgInfo = data.orgInfo;
  // Initialize EventListeners
  eventListeners();
};

// ZOHO Initializers

ZOHO.embeddedApp.on("PageLoad", init);

ZOHO.embeddedApp.init();
