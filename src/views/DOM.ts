const DOM = {
  // Page
  pageSearch: document.querySelector(".page-search") as HTMLDivElement,
  pageCreate: document.querySelector(".page-create") as HTMLDivElement,
  // Forms
  formSearch: document.querySelector(".form-search") as HTMLFormElement,
  formCreate: document.querySelector(".form-create") as HTMLFormElement,
  // Inputs
  searchEmail: document.querySelector("#search_email") as HTMLInputElement,
  searchMobile: document.querySelector("#search_mobile") as HTMLInputElement,
  name: document.querySelector("#name") as HTMLInputElement,
  email: document.querySelector("#email") as HTMLInputElement,
  mobile: document.querySelector("#mobile") as HTMLInputElement,
  nif: document.querySelector("#nif") as HTMLInputElement,
  birthday: document.querySelector("#birthday") as HTMLInputElement,
  // Buttons
  btnSearch: document.querySelector(".btn-search") as HTMLButtonElement,
  btnBack: document.querySelector(".btn-back") as HTMLButtonElement,
  btnCreateRecord: document.querySelector(
    ".btn-createRecord"
  ) as HTMLButtonElement,
};

export default DOM;
